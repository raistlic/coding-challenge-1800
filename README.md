### 1-800 Coding Challenge Solution

##### Project Structure
- the project is a single module gradle project with gradle wrapper
- source folder structure follows maven convention
- project root directory is a (local) git repository
- once built, jacoco report available: `build/reports/jacoco/index.html`

##### Tech Stack
- JDK8 for main implementation, no third party library used
- groovy + spock for unit test
- jacoco for test coverage report

##### Build & Execution Instruction

- requirement: JDK8, Internet connection
- in project root directory run `./gradlew clean build` (or `gradlew clean build` on Windows) to build the project
- once done, jar file available: `build/libs/coding-challenge-1800.jar`
- command to run the jar file follows the challenge specification:
```
java -jar coding-challenge-1800.jar [phone_number_file] [-d dictionary_file]
```
- when phone number file not specified, program will prompt for input from stdin
- when dictionary file not specified, a default dictionary file zipped in the jar will be used