package com.aconex.coding.challenge.util

import spock.lang.Specification
import spock.lang.Unroll

class ArgumentsSpec extends Specification {

    void 'parse(String[] args) validates parameters and throws exception when args is null'() {
        when:
        Arguments.parse(null)

        then:
        thrown IllegalArgumentException
    }

    @Unroll
    void 'parse(String[] args) validates usage and throws exception when #senario'() {
        when:
        Arguments.parse(args)

        then:
        thrown CommandLineParseException

        where:
        senario                                    | args
        'multiple -d args'                         | ['-d', '-d'] as String[]
        'no dictionary file specified after -d'    | ['phoneNumber.txt', '-d'] as String[]
        'more than one dictionary files specified' | ['-d', 'dictionary1.txt', '-d', 'dictionary2.txt'] as String[]
        'same dictionary file specified twice'     | ['-d', 'dictionary1.txt', '-d', 'dictionary1.txt'] as String[]
    }

    void 'parse(String[] args) creates valid instance when no args specified'() {
        when:
        Arguments result = Arguments.parse([] as String[])

        then:
        result != null
        !result.dictionaryFile.isPresent()
        !result.phoneNumberFile.isPresent()
    }

    void 'parse(String[] args) creates valid instance when only phoneNumber file specified'() {
        when:
        String phoneNumberFile = UUID.randomUUID().toString()
        Arguments result = Arguments.parse([phoneNumberFile] as String[])

        then:
        result != null
        !result.dictionaryFile.isPresent()
        result.phoneNumberFile.isPresent()
        result.phoneNumberFile.get() == phoneNumberFile
    }

    void 'parse(String[] args) creates valid instance when only dictionary file specified'() {
        when:
        String dictionaryFile = UUID.randomUUID().toString()
        Arguments result = Arguments.parse(['-d', dictionaryFile] as String[])

        then:
        result != null
        result.dictionaryFile.isPresent()
        result.dictionaryFile.get() == dictionaryFile
        !result.phoneNumberFile.isPresent()
    }

    @Unroll
    void 'parse(String[] args) creates valid instance when both files specified and #senario'() {
        when:
        Arguments result = Arguments.parse(args as String[])

        then:
        result != null
        result.dictionaryFile.isPresent()
        result.dictionaryFile.get() == 'dictionaryFile'
        result.phoneNumberFile.isPresent()
        result.phoneNumberFile.get() == 'phoneNumberFile'

        where:
        senario                                | args
        'phone number file is specified first' | ['phoneNumberFile', '-d', 'dictionaryFile']
        'dictionary file is specified first'   | ['-d', 'dictionaryFile', 'phoneNumberFile']
    }
}