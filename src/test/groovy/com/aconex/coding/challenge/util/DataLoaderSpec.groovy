package com.aconex.coding.challenge.util

import spock.lang.Shared
import spock.lang.Specification

class DataLoaderSpec extends Specification {

    @Shared
    private DataLoader dataLoader = new DataLoader()

    @Shared
    private String fixturePhoneNumber = '324530459'

    void setup() {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(fixturePhoneNumber.bytes)
        System.setIn(byteArrayInputStream)
    }

    void cleanup() {
        System.setIn(System.in)
    }

    void 'loadPhoneNumber(Arguments arguments) validates parameters and throws exception when arguments is null'() {
        when:
        dataLoader.loadPhoneNumber(null)

        then:
        thrown IllegalArgumentException
    }

    void 'loadPhoneNumber(Arguments arguments) loads phone number from file when it is specified'() {
        given:
        Arguments arguments = Mock(Arguments) {
            getPhoneNumberFile() >> Optional.of(getClass().getResource('/test_phone_number.txt').toURI().toURL().file)
        }

        expect:
        dataLoader.loadPhoneNumber(arguments) == '123434654356'
    }

    void 'loadPhoneNumber(Arguments arguments) loads phone number from stdin when it is not specified'() {
        given:
        Arguments arguments = Mock(Arguments) {
            getPhoneNumberFile() >> Optional.empty()
        }

        expect:
        dataLoader.loadPhoneNumber(arguments) == fixturePhoneNumber
    }

    void 'loadDictionary(Arguments arguments) validates parameters and throws exception when arguments is null'() {
        when:
        dataLoader.loadDictionary(null)

        then:
        thrown IllegalArgumentException
    }

    void 'loadDictionary(Arguments arguments) loads dictionary from default file when it is not specified'() {
        given:
        Arguments arguments = Mock(Arguments) {
            getDictionaryFile() >> Optional.empty()
        }

        when:
        Set<String> result = dataLoader.loadDictionary(arguments)

        then:
        result.size() == 999
        result.containsAll(['meant', 'quotient', 'teeth', 'shell', 'neck'])
    }

    void 'loadDictionary(Arguments arguments) loads dictionary from specified file'() {
        given:
        Arguments arguments = Mock(Arguments) {
            getDictionaryFile() >> Optional.of(getClass().getResource('/test_dictionary.txt').toURI().toURL().file)
        }

        expect:
        dataLoader.loadDictionary(arguments) == ['some', 'test', 'words'] as Set<String>
    }
}