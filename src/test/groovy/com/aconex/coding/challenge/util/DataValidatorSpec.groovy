package com.aconex.coding.challenge.util

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class DataValidatorSpec extends Specification {

    @Shared
    private DataValidator dataValidator = new DataValidator()

    void 'validatePhoneNumber(String phoneNumber) validates parameters and throws exception when phoneNumber is null'() {
        when:
        dataValidator.validatePhoneNumber(null)

        then:
        thrown IllegalArgumentException
    }

    @Unroll
    void 'validatePhoneNumber(String phoneNumber) throws exception when #senario'() {
        when:
        dataValidator.validatePhoneNumber(phoneNumber)

        then:
        thrown DataValidationException

        where:
        senario                                  | phoneNumber
        'phoneNumber is empty'                   | ''
        'phoneNumber contains invalid character' | '123a'
    }

    void 'validatePhoneNumber(String phoneNumber) does not throw exception when phoneNumber is valid'() {
        when:
        dataValidator.validatePhoneNumber('23984523764')

        then:
        noExceptionThrown()
    }

    void 'validateWord(String word) validates parameters and throws exception when word is null'() {
        when:
        dataValidator.validateWord(null)

        then:
        thrown IllegalArgumentException
    }

    @Unroll
    void 'validateWord(String word) throws exception when #senario'() {
        when:
        dataValidator.validateWord(word)

        then:
        thrown DataValidationException

        where:
        senario                           | word
        'word is empty'                   | ''
        'word contains numeric character' | 'abc1'
        'word contains invalid character' | 'abc_'
    }

    void 'validateWord(String word) does not throw exception when word is valid'() {
        when:
        dataValidator.validateWord('abc')

        then:
        noExceptionThrown()
    }
}