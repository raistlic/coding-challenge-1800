package com.aconex.coding.challenge.engine

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll


class DictNodeSpec extends Specification {

    @Shared
    private DictNode dictNode = DictNode.createDictionary(['foo', 'bar', 'car'] as Set<String>)

    @Unroll
    void 'factory method validates parameters and throws exception when #senario'() {
        when:
        DictNode.createDictionary(words)

        then:
        thrown IllegalArgumentException

        where:
        senario          | words
        'words is null'  | null
        'words is empty' | [] as Set<String>
    }

    @Unroll
    void 'getNodeByDigit(char digit) validates parameters and throws exception when given char is not digit: #digit'() {
        when:
        dictNode.getNodeByDigit(digit as char)

        then:
        thrown IllegalArgumentException

        where:
        digit << ['a', '_', ' ', '-']
    }

    void 'getNodeByDigit(char digit) returns null when node not found'() {
        expect:
        dictNode.getNodeByDigit('1' as char) == null
    }

    void 'getNodeByDigit(char digit) returns node found'() {
        expect:
        dictNode.getNodeByDigit('3' as char) != null
    }

    void 'getNodeByDigit(char digit) and getWords() finds expected words with correct path'() {
        expect:
        dictNode.getNodeByDigit('3' as char)
            .getNodeByDigit('6' as char)
            .getNodeByDigit('6' as char)
            .words == ['foo'] as List<String>
    }

    void 'getWords() returns all words for a matching path'() {
        when:
        List<String> result = dictNode.getNodeByDigit('2' as char)
            .getNodeByDigit('2' as char)
            .getNodeByDigit('7' as char)
            .words

        then:
        result.size() == 2
        result.containsAll(['bar', 'car'])
    }

    void 'getWords() returns empty List when node has no words'() {
        expect:
        dictNode.words.isEmpty()
    }
}