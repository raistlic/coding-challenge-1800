package com.aconex.coding.challenge.engine

import spock.lang.Specification
import spock.lang.Unroll


class CharMapSpec extends Specification {

    @Unroll
    void 'alphaToDigitOffset(char alpha) validates parameters and throws exception for alpha: #alpha'() {
        when:
        CharMap.alphaToDigitOffset(alpha as char)

        then:
        thrown IllegalArgumentException

        where:
        alpha << ['1', '2', '3', '_', '@', '~', ' ', '-']
    }

    @Unroll
    void 'alphaToDigitOffset(char alpha) returns correct value #value for alpha #alpha'() {
        expect:
        CharMap.alphaToDigitOffset(alpha as char) == value

        where:
        alpha | value
        'a'   | 0
        'b'   | 0
        'c'   | 0
        'd'   | 1
        'e'   | 1
        'f'   | 1
        'g'   | 2
        'h'   | 2
        'i'   | 2
        'j'   | 3
        'k'   | 3
        'l'   | 3
        'm'   | 4
        'n'   | 4
        'o'   | 4
        'p'   | 5
        'q'   | 5
        'r'   | 5
        's'   | 5
        't'   | 6
        'u'   | 6
        'v'   | 6
        'w'   | 7
        'x'   | 7
        'y'   | 7
        'z'   | 7
        'A'   | 0
        'B'   | 0
        'C'   | 0
        'D'   | 1
        'E'   | 1
        'F'   | 1
        'G'   | 2
        'H'   | 2
        'I'   | 2
        'J'   | 3
        'K'   | 3
        'L'   | 3
        'M'   | 4
        'N'   | 4
        'O'   | 4
        'P'   | 5
        'Q'   | 5
        'R'   | 5
        'S'   | 5
        'T'   | 6
        'U'   | 6
        'V'   | 6
        'W'   | 7
        'X'   | 7
        'Y'   | 7
        'Z'   | 7
    }

    @Unroll
    void 'digitOffset(char digit) validates parameters and throws exception for invalid digit: #digit'() {
        when:
        CharMap.digitOffset(digit as char)

        then:
        thrown IllegalArgumentException

        where:
        digit << ['a', 'b', 'c', '_', '@', '~', ' ', '-']
    }

    @Unroll
    void 'digitOffset(char digit) returns correct value for #digit'() {
        expect:
        CharMap.digitOffset(digit as char) == value

        where:
        digit | value
        '0'   | CharMap.NONE
        '1'   | CharMap.NONE
        '2'   | 0
        '3'   | 1
        '4'   | 2
        '5'   | 3
        '6'   | 4
        '7'   | 5
        '8'   | 6
        '9'   | 7
    }
}