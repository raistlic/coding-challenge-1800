package com.aconex.coding.challenge

import spock.lang.Specification

import java.util.stream.Collectors


class MainIntegrationSpec extends Specification {

    private ByteArrayOutputStream outputStream

    void setup() {
        outputStream = new ByteArrayOutputStream()
        System.setOut(new PrintStream(outputStream as OutputStream))
    }

    void cleanup() {
        System.setOut(System.out)
    }

    void 'it should print out matched words with default dictionary file'() {
        given:
        String phoneNumberFile = getClass().getResource('/test_phone_number2.txt').file

        when:
        Main.main(phoneNumberFile)
        Set<String> outputResult = outputStream.toString().split('\n')
            .collect()
            .stream()
            .map({ it.trim() })
            .collect(Collectors.toSet())

        then:
        outputResult == [
            'ON-THIN-I',
            'ON-8-HIM-I',
            'ON-8-I-I-OH',
            'NO-8-HIM-I',
            'NOTHING',
            'ON-8-I-4-OH',
            'ON-8-I-I-6-I',
            'ON-THING',
            'NO-8-I-I-OH',
            'NO-THING',
            'NO-8-I-GO-I',
            'NO-8-I-4-OH',
            'NO-8-I-I-6-I',
            'NO-8-I-IN-I',
            'ON-8-I-GO-I',
            'NO-THIN-I',
            'ON-8-I-IN-I'
        ] as Set<String>
    }

    void 'it should print out matched words with specified dictionary file'() {
        given:
        String dictionaryFile = getClass().getResource('/test_dictionary.txt').file
        String phoneNumberFile = getClass().getResource('/test_phone_number3.txt').file

        when:
        Main.main(phoneNumberFile, '-d', dictionaryFile)
        Set<String> outputResult = outputStream.toString().split('\n')
            .collect()
            .stream()
            .map({ it.trim() })
            .collect(Collectors.toSet())

        then:
        outputResult == ['WORDS'] as Set<String>
    }

    void 'it should have no output when no matching words found'() {
        given:
        String dictionaryFile = getClass().getResource('/test_dictionary.txt').file
        String phoneNumberFile = getClass().getResource('/test_phone_number2.txt').file

        when:
        Main.main(phoneNumberFile, '-d', dictionaryFile)

        then:
        outputStream.toString().isEmpty()
    }
}