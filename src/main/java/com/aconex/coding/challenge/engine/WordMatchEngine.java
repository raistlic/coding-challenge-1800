package com.aconex.coding.challenge.engine;

import java.util.HashSet;
import java.util.Set;

public class WordMatchEngine {

    private final DictNode dictionary;

    public WordMatchEngine(Set<String> words) {

        dictionary = DictNode.createDictionary(words);
    }

    public Set<String> matchWords(String phoneNumber) {

        if (phoneNumber == null) {
            throw new IllegalArgumentException("phoneNumber cannot be null");
        }
        return matchRecursiveSplit(phoneNumber, 0, phoneNumber.length());
    }

    private Set<String> matchRecursiveSplit(String phoneNumber, int offset, int length) {

        Set<String> result = matchAllowOneDigit(phoneNumber, offset, length);
        for (int i = 1; i < length; i++) {
            Set<String> leftMatches = matchRecursiveSplit(phoneNumber, offset, i);
            Set<String> rightMatches = matchRecursiveSplit(phoneNumber, offset + i, length - i);
            for (String left : leftMatches) {
                boolean leftEndsWithDigit = Character.isDigit(left.charAt(left.length() - 1));
                if (left.length() == 1 && leftEndsWithDigit) {
                    continue;
                }
                for (String right : rightMatches) {
                    boolean rightStartsWithDigit = Character.isDigit(right.charAt(0));
                    if (leftEndsWithDigit && rightStartsWithDigit) {
                        continue;
                    }
                    if (rightStartsWithDigit && right.length() == 1) {
                        continue;
                    }
                    result.add(left + "-" + right);
                }
            }
        }
        return result;
    }

    private Set<String> matchAllowOneDigit(String phoneNumber, int offset, int length) {

        Set<String> result = matchExact(phoneNumber, offset, length);
        if (length - 1 > 0) {
            matchExact(phoneNumber, offset + 1, length - 1).stream()
                .map(partialMatch -> phoneNumber.charAt(offset) + "-" + partialMatch)
                .forEach(result::add);
        } else {
            result.add(String.valueOf(phoneNumber.charAt(offset)));
        }
        return result;
    }

    private Set<String> matchExact(String phoneNumber, int offset, int length) {

        DictNode node = dictionary;

        for (int i = offset, limit = offset + length; i < limit; i++) {
            char digit = phoneNumber.charAt(i);
            node = node.getNodeByDigit(digit);
            if (node == null) {
                return new HashSet<>();
            }
        }
        return new HashSet<>(node.getWords());
    }
}
