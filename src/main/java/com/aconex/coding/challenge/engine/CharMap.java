package com.aconex.coding.challenge.engine;

/**
 * This class helps translating alphabetical character into a digit character or its corresponding offset index.
 */
class CharMap {

    /**
     * The total number of digit characters that have alphabetical mapping.
     */
    static final int DIGITS_COUNT = "23456789".length();

    /**
     * The special index value that indicates a digit is out of range (not mapped to any character).
     */
    static final int NONE = -1;

    /**
     * Maps the specified alphabetical character into an offset index of a digit.
     *
     * @param alpha the character to map, must be an alphabetical character, case insensitive.
     * @return the mapped offset
     *
     * @throws IllegalArgumentException when the given {@code alpha} is not an alphabetical char.
     */
    static int alphaToDigitOffset(char alpha) {

        if (!Character.isAlphabetic(alpha)) {
            throw new IllegalArgumentException("Not an alphabetical character: " + alpha);
        }

        char digit = MAP[Character.toUpperCase(alpha) - 'A'];
        return digitOffset(digit);
    }

    /**
     * Returns the corresponding offset for specified {@code digit} char.
     *
     * @param digit the digit char to get offset for
     * @return the offset, or {@link #NONE} if the digit is out of bounds (not mapped to any alphabetical characters).
     *
     * @throws IllegalArgumentException when the given {@code digit} is not a digit character.
     */
    static int digitOffset(char digit) {

        if (!Character.isDigit(digit)) {
            throw new IllegalArgumentException("Not a digit character: " + digit);
        }

        int offset = digit - OFFSET_ZERO;
        return offset < 0 ? NONE : offset;
    }

    private static final char OFFSET_ZERO = '2';

    private static final char[] MAP = new char[26];

    static {
        MAP['A' - 'A'] = '2';
        MAP['B' - 'A'] = '2';
        MAP['C' - 'A'] = '2';

        MAP['D' - 'A'] = '3';
        MAP['E' - 'A'] = '3';
        MAP['F' - 'A'] = '3';

        MAP['G' - 'A'] = '4';
        MAP['H' - 'A'] = '4';
        MAP['I' - 'A'] = '4';

        MAP['J' - 'A'] = '5';
        MAP['K' - 'A'] = '5';
        MAP['L' - 'A'] = '5';

        MAP['M' - 'A'] = '6';
        MAP['N' - 'A'] = '6';
        MAP['O' - 'A'] = '6';

        MAP['P' - 'A'] = '7';
        MAP['Q' - 'A'] = '7';
        MAP['R' - 'A'] = '7';
        MAP['S' - 'A'] = '7';

        MAP['T' - 'A'] = '8';
        MAP['U' - 'A'] = '8';
        MAP['V' - 'A'] = '8';

        MAP['W' - 'A'] = '9';
        MAP['X' - 'A'] = '9';
        MAP['Y' - 'A'] = '9';
        MAP['Z' - 'A'] = '9';
    }
}
