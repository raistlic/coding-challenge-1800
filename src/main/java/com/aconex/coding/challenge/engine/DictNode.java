package com.aconex.coding.challenge.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Dictionary as a tree structure.
 */
class DictNode {

    static DictNode createDictionary(Set<String> words) {

        if (words == null) {
            throw new IllegalArgumentException("words cannot be null");
        }
        if (words.isEmpty()) {
            throw new IllegalArgumentException("words cannot be empty");
        }

        DictNode dictionary = new DictNode();

        for (String word : words) {
            DictNode node = dictionary;
            for (int i = 0, len = word.length(); i < len; i++) {
                char alphabetical = word.charAt(i);
                node = node.touchNodeByAlpha(alphabetical);
            }
            node.addWord(word);
        }
        return dictionary;
    }

    private DictNode[] nodes;

    private List<String> words;

    private DictNode() {

        this.nodes = new DictNode[CharMap.DIGITS_COUNT];
        this.words = null;
    }

    private void addWord(String word) {

        if (this.words == null) {
            this.words = new ArrayList<>();
        }
        this.words.add(word);
    }

    private DictNode touchNodeByAlpha(char alpha) {

        int index = CharMap.alphaToDigitOffset(alpha);
        if (nodes[index] == null) {
            nodes[index] = new DictNode();
        }
        return nodes[index];
    }

    DictNode getNodeByDigit(char digit) {

        int index = CharMap.digitOffset(digit);
        return index == CharMap.NONE ? null : nodes[index];
    }

    List<String> getWords() {

        return words == null ? Collections.emptyList() : words;
    }
}
