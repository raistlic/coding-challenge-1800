package com.aconex.coding.challenge.util;

/**
 * The dedicated exception type for any util line parse/validation errors.
 */
public class CommandLineParseException extends RuntimeException {

    public CommandLineParseException(String message) {

        super(message);
    }
}
