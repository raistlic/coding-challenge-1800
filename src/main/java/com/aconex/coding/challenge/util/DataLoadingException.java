package com.aconex.coding.challenge.util;

public class DataLoadingException extends RuntimeException {

    public DataLoadingException(String message) {

        super(message);
    }

    public DataLoadingException(String message, Throwable cause) {

        super(message, cause);
    }
}
