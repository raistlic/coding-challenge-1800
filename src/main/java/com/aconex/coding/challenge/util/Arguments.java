package com.aconex.coding.challenge.util;

import java.util.Optional;

/**
 * Helps to parse, validate util line arguments and provide code friendly queries.
 */
public class Arguments {

    private static final String USAGE = "USAGE: java -jar coding-challenge-1800.jar [phoneNumberFile] [-d dictionaryFile]";

    /**
     * Parses the util line arguments and creates an {@link Arguments} instance to hold the parsed information.
     *
     * @param args the util line arguments, cannot be {@code null}.
     * @return the parsed result.
     *
     * @throws IllegalArgumentException  when args is {@code null}.
     * @throws CommandLineParseException when the {@code args} are invalid usage.
     */
    public static Arguments parse(String[] args) {

        if (args == null) {
            throw new IllegalArgumentException("args cannot be null");
        }

        String dictionaryFile = null;
        String phoneNumberFile = null;
        boolean dictionaryFlag = false;

        for (String arg : args) {
            if (arg.trim().toLowerCase().equals("-d")) {
                if (dictionaryFlag || dictionaryFile != null) {
                    throw new CommandLineParseException(USAGE);
                } else {
                    dictionaryFlag = true;
                }
            } else if (dictionaryFlag) {
                dictionaryFile = arg.trim();
                dictionaryFlag = false;
            } else {
                phoneNumberFile = arg.trim();
            }
        }

        if (dictionaryFlag) {
            throw new CommandLineParseException(USAGE);
        }

        return new Arguments(phoneNumberFile, dictionaryFile);
    }

    private final String phoneNumberFile;

    private final String dictionaryFile;

    private Arguments(String phoneNumberFile, String dictionaryFile) {

        this.phoneNumberFile = phoneNumberFile;
        this.dictionaryFile = dictionaryFile;
    }

    public Optional<String> getDictionaryFile() {

        return Optional.ofNullable(dictionaryFile);
    }

    public Optional<String> getPhoneNumberFile() {

        return Optional.ofNullable(phoneNumberFile);
    }
}
