package com.aconex.coding.challenge.util;

/**
 * Helper class to validate user input.
 */
public class DataValidator {

    /**
     * A phone number can not be empty, and can have only numeric characters.
     *
     * @param phoneNumber the phone number to validate, cannot be {@code null}.
     * @throws IllegalArgumentException when {@code phoneNumber} is {@code null}.
     * @throws DataValidationException  when {@code phoneNumber} is invalid.
     */
    public void validatePhoneNumber(String phoneNumber) {

        if (phoneNumber == null) {
            throw new IllegalArgumentException("phone number cannot be null");
        }

        if (phoneNumber.isEmpty()) {
            throw new DataValidationException("invalid phone number: empty");
        }
        for (int i = 0, len = phoneNumber.length(); i < len; i++) {
            if (!Character.isDigit(phoneNumber.charAt(i))) {
                throw new DataValidationException(String.format("invalid phone number: '%s'", phoneNumber));
            }
        }
    }

    /**
     * A word cannot be empty, and can have only alphabetical characters.
     *
     * @param word to be validated, cannot be {@code null}.
     * @throws IllegalArgumentException when {@code word} is {@code null}.
     * @throws DataValidationException  when {@code word} is invalid.
     */
    public void validateWord(String word) {

        if (word == null) {
            throw new IllegalArgumentException("word cannot be null.");
        }

        if (word.isEmpty()) {
            throw new DataValidationException("invalid word: empty");
        }
        for (int i = 0, len = word.length(); i < len; i++) {
            if (!Character.isAlphabetic(word.charAt(i))) {
                throw new DataValidationException(String.format("invalid word in dictionary: '%s'", word));
            }
        }
    }
}
