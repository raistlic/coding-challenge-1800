package com.aconex.coding.challenge.util;

import com.aconex.coding.challenge.Constants;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

/**
 * The utility class that reads phone number and dictionary data.
 */
public class DataLoader {

    private static final String IGNORED_CHARACTERS_REGEX = "[\\\\._']";

    /**
     * Load phone number from the phone number file if it is specified in {@code arguemnts} , or else load it from stdin.
     *
     * @param arguments the parsed command line arguments, cannot be {@code null}.
     * @return the phone number loaded.
     *
     * @throws IllegalArgumentException when {@code arguments} is {@code null}.
     */
    public String loadPhoneNumber(Arguments arguments) {

        if (arguments == null) {
            throw new IllegalArgumentException("arguments cannot be null");
        }

        try (InputStream in = arguments.getPhoneNumberFile()
            .map(this::openFile)
            .orElse(System.in)) {

            String line = new BufferedReader(new InputStreamReader(in)).readLine();
            if (line == null) {
                throw new DataLoadingException("Failed loading phone number: no content");
            }
            return line.replaceAll(IGNORED_CHARACTERS_REGEX, "").trim();

        } catch (IOException ex) {
            throw new DataLoadingException("Failed loading phone number: " + ex.getMessage(), ex);
        }
    }

    /**
     * Load dictionary words from the dictionary file if it is specified in {@code arguemnts} , or else load it from
     * the default dictionary file.
     *
     * @param arguments the parsed command line arguments, cannot be {@code null}.
     * @return the phone number loaded.
     *
     * @throws IllegalArgumentException when {@code arguments} is {@code null}.
     */
    public Set<String> loadDictionary(Arguments arguments) {

        if (arguments == null) {
            throw new IllegalArgumentException("arguments cannot be null");
        }

        try (InputStream in = arguments.getDictionaryFile()
            .map(this::openFile)
            .orElse(getClass().getResourceAsStream(Constants.DEFAULT_DICTIONARY_FILE))) {

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            Set<String> dictionary = new HashSet<>();
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                dictionary.add(line.replaceAll(IGNORED_CHARACTERS_REGEX, "").trim());
            }
            return dictionary;
        } catch (IOException ex) {
            throw new DataLoadingException("Failed loading dictionary data: " + ex.getMessage(), ex);
        }
    }

    private InputStream openFile(String file) {

        try {
            return new FileInputStream(new File(file));
        } catch (FileNotFoundException ex) {
            throw new DataLoadingException(ex.getMessage(), ex);
        }
    }
}
