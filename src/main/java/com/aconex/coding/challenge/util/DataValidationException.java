package com.aconex.coding.challenge.util;

public class DataValidationException extends RuntimeException {

    public DataValidationException(String message) {

        super(message);
    }
}
