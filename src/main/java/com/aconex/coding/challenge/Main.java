package com.aconex.coding.challenge;

import com.aconex.coding.challenge.engine.WordMatchEngine;
import com.aconex.coding.challenge.util.Arguments;
import com.aconex.coding.challenge.util.CommandLineParseException;
import com.aconex.coding.challenge.util.DataLoader;
import com.aconex.coding.challenge.util.DataValidationException;
import com.aconex.coding.challenge.util.DataValidator;

import java.util.Set;

public class Main {

    public static void main(String[] args) {

        try {

            Arguments arguments = Arguments.parse(args);
            DataLoader dataLoader = new DataLoader();
            DataValidator dataValidator = new DataValidator();

            String phoneNumber = dataLoader.loadPhoneNumber(arguments);
            dataValidator.validatePhoneNumber(phoneNumber);

            Set<String> words = dataLoader.loadDictionary(arguments);
            if (words.isEmpty()) {
                throw new DataValidationException("No words found in dictionary file.");
            }
            words.forEach(dataValidator::validateWord);

            WordMatchEngine wordMatchEngine = new WordMatchEngine(words);
            wordMatchEngine.matchWords(phoneNumber).stream()
                .map(String::toUpperCase)
                .forEach(System.out::println);

        } catch (CommandLineParseException | DataValidationException ex) {

            System.err.println(ex.getMessage());
        }
    }
}
