package com.aconex.coding.challenge;

public final class Constants {

    public static final String DEFAULT_DICTIONARY_FILE = "/default_dictionary.txt";

    private Constants() {

    }
}
